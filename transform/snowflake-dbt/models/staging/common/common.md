{% docs dim_customers %}
Dimensional customer table representing all currently existing and historical customers
as defined in the [handbook](https://about.gitlab.com/handbook/sales/#customer)
{% enddocs %}

{% docs fct_invoice_items %}
Invoice items joined with invoice to give more context to each invoice item.  Data comes from Zuora.  See https://knowledgecenter.zuora.com/Billing/Reporting_and_Analytics/D_Data_Sources_and_Exports/C_Data_Source_Reference/Invoice_Item_Data_Source .

{% enddocs %}